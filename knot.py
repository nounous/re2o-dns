import subprocess


def get_ds(zone, verbose=False):
    if verbose:
        print("Getting CDS of %s:" % (zone,))
        print("/usr/sbin/knotc zone-read %s @ CDS" % (zone,))
    try:
        cdss = subprocess.check_output(['/usr/sbin/knotc', 'zone-read', zone, '@', 'CDS'])[:-1].decode('utf-8').split('\n')
    except subprocess.CalledProcessError:
        return []
    dss = []
    if verbose:
        print("CDS of %s = %s" % (zone, cdss))
    for cds in cdss:
        ds = {}
        try:
            cds = cds.split(' ')
            ds['subzone'] = cds[1]
            ds['id'] = cds[4]
            ds['algo'] = cds[5]
            ds['type'] = cds[6]
            ds['fp'] = cds[7]
        except:
            if verbose:
                print('Unable to find ksk for', zone)
            continue
        ds['ttl'] = 172800
        if verbose:
            print("DS record of %s : %s" % (zone, ds))
            print("\n\n")
        dss.append(ds)
    return dss
