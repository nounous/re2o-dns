## Re2o - DNS

This service uses Re2o API to generate DNS zone files


## Requirements

* python3
* knot
* requirements in https://gitlab.federez.net/re2o/re2oapi

## Scripts

* `main.py`: Generates the zone files and reloads the zones
* `dnssec_generate.py`: Generate the DS records for the zones in `dnssec_domains.json` and writes them to `dnssec.json`
